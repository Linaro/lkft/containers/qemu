FROM debian:sid-slim
RUN apt-get update \
    && apt-get install -qy auto-apt-proxy \
    && apt-get install -qy \
      qemu-system-arm \
      qemu-system-common \
      qemu-system-data \
      qemu-system-gui \
      qemu-system-mips \
      qemu-system-misc \
      qemu-system-ppc \
      qemu-system-sparc \
      qemu-system-x86 \
      --no-install-recommends
